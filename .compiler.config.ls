preset = require 'livescript-preset-experimental' .__default__
{ plugins } = preset

module.exports =
    plugins: plugins.filter (.plugin.name != 'transform-top-level-await')
