import \assert

bar = -> Promise.resolve \bar

x = "foo #{await bar!}"
assert x == 'foo bar'
