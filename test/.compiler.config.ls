preset = require 'livescript-preset-experimental' .__default__
transform = require '../lib/plugin' .__default__
{ plugins } = preset

module.exports =
    plugins: plugins.filter (.plugin.name != 'transform-top-level-await')

module.exports.plugins.push plugin: transform

# console.log module.exports.plugins
