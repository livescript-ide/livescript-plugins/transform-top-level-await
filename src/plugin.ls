import
    \livescript-compiler/lib/livescript/Plugin

export default transform-top-level-await = ^^Plugin

    ..name = 'transform-top-level-await'

    ..override-options = true

    ..enable = !->
        { Block } = @livescript.livescript.ast
        original-compile-root = Block::compile-root
            @original-compile-root  = ..

        Self = @
        Block::compile-root = (options) ->
            is-async-block = has-await-in @
            # livescript has not so great documentation so I don't know how
            # to set default bare to false
            # so we need to override it manualy if we are running async code
            if is-async-block and Self.override-options
                options.bare = false
            if is-async-block and options.bare == true
                throw Error "Top level code should be wrap in async function but compile option 'bare' is preventing it."

            result = original-compile-root.call @, ...&
                ..children.0 = '(async function(){\n' if is-async-block

    ..disable = !->
        @livescript.ast.Block::compile-root = @original-compile-root


# check if inside a block there is an await expression
# TODO: refactor this to more generic function used for searching nodes
has-await-in = (block) ->
    result = null
    is-await = (child, parent, name, index) ->
        # there is no separate node for Await so we are checking for
        # property 'op' equal to 'await'
        if child.op == 'await'
            result := true
        # I'm guesing that returning here not null value will stop futher traversing
        result

    block.traverse-children is-await, false
    result ? false
