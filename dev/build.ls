import
    \livescript-system : system

system
    ..watch = false
    ..clean = true
    ..build!
